In Sonic the Hedgehog, which island does it take place on?
South Island
Angel Island
Flicky Island
Christmas Island
A
In Hyperdimension Neptunia, which of the following is not a goddess of Planeptune?
Neptune
Nepgear
Histoire
Plutia
X
On The Super Mario Bros. Super Show, what was Princess's name?
Peach
Toadstool
Pauline
Daisy
B
In Five Nights at Freddy's, who has been theorized to have killed the five children?
Mike Schmidt
Jeremy Fitzgerld
Fritz Smith
Phone Guy
Y
In Undertale, which mountain does the game take place?
Cool, Cool Mountain
Mount Ebott
Red Mountain
Mount Sinai
B
In Sonic the Comic, what caused Sonic to turn blue and created Eggman?
Chaos Energy Cannon
Eclipse Cannon
Genesis Wave
Retro-Orbital Chaos Compressor
Y
In The Mask Comic, what is the wearer of the mask called?
The Mask
Loki
The Masque
Big-Head
Y
In Detective Comic, the Joker original started off as who?
Red Hood
The Man Who Laughs
Jack
Jerome Valeska
A
In Amazing Fantasy Comic, what bit Peter Parker to turn him into Spider-Man?
Ant
Bat
Spider
Turtle
X
In Dragon Ball Z: Battle of Gods, what is the strongest form a Saiyan can reach?
Super Saiyan 4
Super Saiyan God
Super Saiyan 3
Super Saiyan 2
B
In Crash Bandicoot, how would you unlock the alternate path?
Rescue all the Electoons
Collect all the Gems
Collect all the Stars
Find all six brightly colored Eggs
B
In Grand Theft Auto Vice City, which of the following cheats gives you a Rhino Tank?
Right, R2, Circle, R1, L2, Square, R1, R2
R2, L2, R1, L1, L2, R2, Square, Triangle, Circle, Triangle, L2, L1
Right, L2, Down, R1, Left, Left, R1, L1, L2, L1
Circle, Circle, L1, Circle, Circle, Circle, L1, L2, R1, Triangle, Circle, Triangle
Y
In Pac-Man: Adventures in Time, which time period wasn't period?
Prehistoric
Middle Ages
Edo Period
Ancient Egypt
X
In Star Fox: Assault, the story pickups where which game left off?
Star Fox Adventures
Star Fox
Star Fox 64
It's Nintendo their is no Story
A
In Epic Mickey, how is Oswald the Lucky Rabbit related to Mickey Mouse?
Brothers
Half-Brothers
Half-Cousins
Cousins
B
On Fresh Prince of Bel Air, how many times did Jazz get thrown out the house?
11 times
12 times
13 times
15 times
Y
On Disney's Paint the Night Parade, which unit is exclusive to Disneyland?
Cars Unit
Frozen Unit
Monsters Inc. Unit
Toy Story Unit
B
On Batman: The Animated Series, who did the voice of the Joker?
Troy Baker
Health Ledger
Mark Hamil
Jack Nicholson
X
On ScrewAttack's Death Battle, which of the following characters lost?
Deadpool
Godzilla
Ryu
Samus
X
On Animaniacs, how many warners are their?
1 warner
2 warners
3 warners
4 warners
X
In Mickey Mouse Get A Horse!, what kind of animation does it use?
Black-and-White Animation
Hand Drawn Animation
CGI Animation
All of Above
Y
In Ghostbusters, what form does Gozer become?
Illinois Nazis
Taxi Driver
Stay Puft Marshmallow Man
The Logo
X
In Asterix the Gaul Comic, which province of Gaul does it take place?
Aedui
Armorica
Treveri
Pictones
B
In Mega Man, how many bosses do you fight in total?
16 bosses
9 bosses
10 bosses
6 bosses
A
On Furious 7, which actor received a tribute at the end?
Harold Remis
James Avery
Paul Walker
Robin Williams
X
On King of the Hill, what is Hank Hill's primary occupation?
Butane Gas Salesman
Jean Salesman
Propane Salesman
Charcoal Salesman
X
On Rich Alvarez's The Plumber Knight Returns, what happened to Luigi?
He became Evil
He Disappeared
He Retired
He was Murdered
Y
On The Powerpuff Girls, who originally did the voice of the Narrator?
Ernie Anderson
Roger L. Jackson
Taiten Kusunoki
Tom Kenny
A
In Pac-Man World 3, Pac-Man is celebrating his Xth anniversary?
15th
20th
25th
30th
X
On Who Framed Roger Rabbit, which character did not appear in the film?
Bugs Bunny
Mickey Mouse
Popeye
Woody Woodpeckor
X
On Wreck-It Ralph, which game is Ralph from?
Turbo Time
Fix-It Felix
Sugar Rush
Hero's Duty
B
In Five Nights at Freddy's 4, what year does it say it take place?
1983
1987
1993
2023
A
In Banjo-Kazooie, why does Gruntilda kidnaps Tooty?
To Get The Money She Owed
To Get Revenge On Her Brother
To Play A Game
To Take Her Beauty
Y
On Disney's Fantasmic!, which character was exclusive to the Disneyland version?
Simba
Pocahontas
Peter Pan
Stitch
X
In Grand Theft Auto San Andreas, which of the following cheats give you a Jetpack?
O, L1, Up, R1, L2, X, R1, L1, O, X
Right, L2, Down, R1, Left, Left, R1, L1, L2, L1
Up, Down, Left, Right, L1, L2, R1, R2, Up, Down, Left, Right
Square, R2, Down, Down, Left, Down, Left, Left, L2, X
X
In Brave New World, why does Thomas resign from his position as Director of Hatcheries and Conditioning?
Anger
Jealousy
Pride
Shame
Y
On Tim Burton's The Nightmare Before Christmas, how many holiday trees are there?
6 trees
7 trees
8 trees
9 trees
B
On The Melancholy of Haruhi Suzumiya, who is the leader of Spreading Cheer to Our Student Body Brigade?
Haruhi Suzumiya
Itsuki Koizumi
Mikuru Asahina
Yuki Nagato
A
In Super Mario Bros, which castle is the princess located?
World 2
World 4
World 6
World 8
Y
In Pac-Man, how far can you go?
122
131
244
256
Y