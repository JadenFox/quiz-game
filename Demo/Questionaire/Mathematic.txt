If Jeremy is 4 and his sister is half his age, at age 100, what age is she?
25
50
75
98
Y
Henry is driving 5 mph, after 3 hours on the road, how fast will he be traveling?
10 mph
15 mph
30 mph
45 mph
B
3x + 15 = 45, what is the value of X?
X = 3
X = 5
X = 8
X = 10
Y
Einstein's famous relativity equation is what?
E = MC^2
E = MCU
E = M^C2
E = MC-2
A
Which of the following isn't a prime number?
31
43
55
67
X
(-4, -4) and (6, 6), what is the rise and run?
(10, 5)
(10, 10)
(5, 10)
(5, 5)
B
What is the value of Earth's gravity?
3.2
4.9
9.8
686
X
Convert 11/20 into a decimal?
.18
.27
.55
.74
X
You have 3300 Life Points, you received 2500 points direct damage, how much Life Points do you have left?
800 LP
700 LP
600 LP
500 LP
A
1025 + 985 = ?
2020
2045
2070
2095
X
3y = 14 - 2, what is the value of Y?
Y = 4
Y = 8
Y = 2
Y = 5
A
Green Hills Zone is 4862 miles, if Sonic ran at 787 mph, how long will it take him to get to the goal?
1.56 hrs
3.22 hrs
6.18 hrs
8.04 hrs
X
What is the value of Pie?
Yummy
3.14
6.35
9.81
B
12 * 5 = ?
15
30
45
60
Y
Which of the following isn't divisible by 5?
100
125
150
177
Y
Convert 75% into a fraction?
1/4
1/2
3/4
1
X
(-2, -6) and (-1, 6), what is the rise and run?
(-1, 12)
(-2, 6)
(6, -2)
(12, -1)
A
It is currently 10:55am, the meeting starts at 11:05am and ends at 3:25p, how much time gone bye by the time the meeting ends?
4 hrs 15 mins
4 hrs 30 mins
4 hrs 45 mins
5 hrs
B
7 - 2 = ?
-5
-2
2
5
Y
What is Multiplication Property of Zero?
anything times itself is always zero
anything times negative is always zero
anything times one is always zero
anything times zero is always zero
Y
It is 3816 miles to the first flag, Mario travels at 7.74 mph, how long will it take him to get there?
you're Princess is in another Castle
246.51 hrs
369.13 hrs
493.02 hrs
Y
7x - 2y = 14, what is the value of X and Y?
X = -2 & Y = 7
X = 2 & Y = -7
X = 4 & Y = -14
X = -4 & Y = 14
B
What is 133 divided by 7?
0.05
0.14
7
19
Y
Which of the following isn't divisible by 2?
12
27
42
60
B
You have 5300 Life Points, you pay half of your Life Points, how much Life Points do you have left?
Screw the Rules, I have Money!
1325
2650
3975
X
(4, -4) and (3, 2), what is the rise and run?
(1, -6)
(-1, 6)
(-6, 1)
(6, -1)
B
It is currently 2:30pm, the new anime premiers at 5:30pm, how long is the wait?
2 hrs
2 hrs 30 mins
3 hrs
3 hrs 30 mins
X
Convert 1.10 into a percentage?
11.0%
.110%
110%
1.10%
X
What is the Zero Property of Addition?
anything that adds zero is always zero
anything that adds zero is always negative
anything that adds zero is always one
anything that adds zero is itself
Y
What is the square root of 25?
Do I look like a scientist to you?
2
4
5
Y
Twice a number is Twenty-Eight?
x + 2 = 28
28 = x/2
2x = 28
None of the Above
X
21^-2
441
1/441
1/-441
-441
B
Harold is driving 45 mph in the country side, the next gas station is 100 miles away, how long will it take him to get there?
2.22 hrs
2.67 hrs
3.12 hrs
3.57 hrs
A
Which of the following isn't divisible by 9?
2115
5310
6082
6147
X
10x (2y - 2), if X = 15 and Y = 7, what is the answer?
900
1250
1800
2250
X
It is currently 6:00pm, the late night movie premier is at 11:05pm, the movie is 1 hr 48 mins, at the end of the day, how long has it been?
6 hrs 30 mins
6 hrs 53 mins
7 hrs 16 mins
7 hrs 39 mins
B
Convert 2/3 into a percent?
66%
99%
132%
165%
A
What is the Multiplicative Identity Property?
anything times itself is always itself
anything times zero is always itself
anything times one is always itself
anything times negative is always itself
X
You have used 25 machine monsters, if the monster in question gets 800 points for each monster used, how much attack and defense will it have?
You are gonna lose, my friend
15000 ATK / 15000 DEF
20000 ATK / 20000 DEF
25000 ATK / 25000 DEF
X
What is the square root of 121?
McDonalds, here I come
5
8
11
Y