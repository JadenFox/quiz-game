﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum phase { ANSWER, CHECK, FINAL, END, }

[System.Serializable]
public struct PlayerDisplay
{
	public string name;
	public Image img;
	public Text num;
	public Text answer;
	public Sprite normal;
	public Sprite right;
	public Sprite wrong;
	public PlayerController player;
	public bool correct;
	public int points;
	public int time;
	public int correctAnswers;

	public void SetDisplayTo(bool t)
	{
		img.enabled = t;
		num.enabled = t;
		answer.enabled = t;
	}

	public void Run(phase ph)
	{
		num.text = "P" + player.playernum + "\n" + points + " pts";
		if (ph == phase.ANSWER)
		{
			if (player.playeranswer == ans.NONE)
				answer.text = " ";
			else
				answer.text = "?";

			img.sprite = normal;
		}

		if (ph == phase.FINAL)
		{
			if (player.playeranswer != ans.NONE)
				answer.text = player.playeranswer.ToString();
			if(correct)
				img.sprite = right;
			else
				img.sprite = wrong;
		}
	}

	public bool isPressed() { return answer.text != " "; }
	public void Subtract(int max) {
		points -= (max - time);
		if(points < 0)
			points = 0;
	}
};

[System.Serializable]
public struct PauseDisplay
{
	public Image img;
	public Text pause;
	
	public void SetDisplayTo(bool t)
	{
		img.enabled = t;
		pause.enabled = t;
	}
};

[System.Serializable]
public struct QuestionDisplay
{
    public Text question;
    public Text atext;
    public Text btext;
    public Text xtext;
    public Text ytext;
	public Image aimg;
	public Image bimg;
	public Image ximg;
	public Image yimg;
	public Sprite normal;
	public Sprite correct;
	public Sprite incorrect;
	public int count;

    public void Set(Question Q)
    {
		count ++;
        question.text = count.ToString() + ") " + Q.Q;
        atext.text = "A\n" + Q.ansA;
        btext.text = "B\n" + Q.ansB;
        xtext.text = "X\n" + Q.ansX;
        ytext.text = "Y\n" + Q.ansY;
		aimg.sprite = normal;
		bimg.sprite = normal;
		ximg.sprite = normal;
		yimg.sprite = normal;
	}
	
	public void Display(Question Q)
	{
		question.text = Q.Q;
		atext.text = "A\n" + Q.ansA;
		btext.text = "B\n" + Q.ansB;
		xtext.text = "X\n" + Q.ansX;
		ytext.text = "Y\n" + Q.ansY;
		aimg.sprite = normal;
		bimg.sprite = normal;
		ximg.sprite = normal;
		yimg.sprite = normal;
	}

	public void GetAnswer(ans a)
	{
		aimg.sprite = incorrect;
		bimg.sprite = incorrect;
		ximg.sprite = incorrect;
		yimg.sprite = incorrect;

		if(a == ans.A)
			aimg.sprite = correct;
		if(a == ans.B)
			bimg.sprite = correct;
		if(a == ans.X)
			ximg.sprite = correct;
		if(a == ans.Y)
			yimg.sprite = correct;
	}
};

[System.Serializable]
public struct TimerDisplay
{
	public Text counter;
	private int _maxtime;
	private int _timer;
	private bool _time;

	private void SetMaxTime(int max) { _maxtime = (max * 60); }
	public void Set(int max)
	{
		SetMaxTime(max);
		_timer = _maxtime;
		_time = false;
	}

	public void Set()
	{
		_timer = _maxtime;
		_time = false;
	}
	
	public void Run()
	{
		if(!_time)
		{
			_timer --;
			if(_timer <= 0) {
				_timer = 0;
				_time = true;
			}
			counter.text = ((_timer / 60)+1).ToString();
		}
	}
	
	public int GetTime() { return ((_timer / 60)+1); }
	public bool isTime() {
		if(_time)
			counter.text = "TIME";
		return _time;
	}
};

public class Gameplay : MonoBehaviour
{
	public AudioSource winSound;
	public AudioSource loseSound;
	public PauseDisplay pause;
	public PauseDisplay winner;
	public TimerDisplay timer;
	public PlayerDisplay[] players;
    public QuestionDisplay question;
	private GameManager _gamemanager;
	private Question _end;
	private int _maxtimer;
    private phase _phase;
	private int _length;
	private bool _win;

	void Start()
	{
		_length = 0;
        _phase = phase.ANSWER;
		_gamemanager = GameObject.Find("GameManager").GetComponent<GameManager>();
		_gamemanager.ChangeGameState(GameManager.state.PLAY);
		for(int i=_gamemanager.GetPlayerAmount(); i < players.Length; i++)
			players[i].SetDisplayTo(false);

		_maxtimer = 10;
		timer.Set(_maxtimer);
		winner.SetDisplayTo(false);
		question.count = 0;
        question.Set(_gamemanager.GetQuestion());
		for(int i = 0; i < _gamemanager.GetPlayerAmount(); ++i) {
			players[i].correctAnswers = 0;
			players[i].correct = false;
			players[i].points = 0;
		}
    }

	void Update()
	{
		pause.SetDisplayTo(_gamemanager.isPaused());
        if(!_gamemanager.isPaused ()) {
			switch (_phase) {
				case phase.ANSWER:
					timer.Run();
					for (int i = 0; i < _gamemanager.GetPlayerAmount(); ++i)
						if(!players[i].isPressed())
							players[i].time = timer.GetTime();
					for (int i = 0; i < _gamemanager.GetPlayerAmount(); ++i)
						players[i].Run (phase.ANSWER);

					if(timer.isTime()) {
						for (int i = 0; i < _gamemanager.GetPlayerAmount(); ++i)
							if(!players[i].isPressed())
								players[i].time = 0;
					}

					if(AllPlayersReady() || timer.isTime())
						ChangePhase (phase.CHECK);

					break;
					
				case phase.CHECK:
					if (_gamemanager.Delay (1))
						ChangePhase (phase.FINAL);
					break;
					
				case phase.FINAL:
					for (int i = 0; i < _gamemanager.GetPlayerAmount(); ++i)
						players[i].Run (phase.FINAL);

					timer.counter.text = "Press Back";
					if(Input.GetButtonDown("Back")) {
						_gamemanager.ChangeGameState (GameManager.state.PLAY);
						_gamemanager.NextQuestion();
						_length ++;
						for (int i = 0; i < _gamemanager.GetPlayerAmount(); ++i) {
							players[i].player.playeranswer = ans.NONE;
							players[i].answer.text = " ";
						}

						if (_length != _gamemanager.GetQuestionLength ())
							ChangePhase (phase.ANSWER);
						else
							ChangePhase (phase.END);
					}
					break;
						
				case phase.END:
					timer.counter.text = "Press Start";
					_gamemanager.ChangeGameState (GameManager.state.END);
					_gamemanager.isPaused ();
					winner.SetDisplayTo (true);
					winner.pause.text = "Winner\n" + GetWinner ();
					if (Input.GetButtonDown ("p1Start")) {
						_gamemanager.ChangeGameState (GameManager.state.MENU);
						Application.LoadLevel (1);
					}
					break;
			}
		} else {
			if(Input.GetButtonDown("Back")) {
				_gamemanager.ClearQuestionaire();
				Application.LoadLevel(1);
			}
		}
	}

	void ChangePhase(phase ph)
	{
		_phase = ph;
		if(ph == phase.ANSWER) {
			_gamemanager.ChangeGameState(GameManager.state.PLAY);
			for(int i = 0; i < _gamemanager.GetPlayerAmount(); ++i) {
				players[i].player.playeranswer = ans.NONE;
				players[i].answer.text = " ";
			}

			question.Set(_gamemanager.GetQuestion());
			timer.Set();
		}

		if(ph == phase.CHECK)
		{
			_win = false;
			for(int i = 0; i < _gamemanager.GetPlayerAmount(); ++i) {
				players[i].correct = _gamemanager.GetQuestion().IsCorrect((players[i].player.playeranswer));
				if(players[i].correct) {
					if(_gamemanager.gametype == GameManager.type.NORMAL)
						players[i].points += players[i].time;
					if(_gamemanager.gametype == GameManager.type.STUDY)
						players[i].points ++;
					_win = true;
				}
				else if(!players[i].correct && _gamemanager.gametype == GameManager.type.CHAOTIC)
					players[i].Subtract(_maxtimer);
			}
		}
		
		if(ph == phase.FINAL) {
			_gamemanager.ChangeGameState(GameManager.state.END);
			for(int p=0; p < _gamemanager.GetPlayerAmount(); ++p)
				players[p].correctAnswers ++;

			if(_win)
				winSound.Play();
			else
				loseSound.Play();

			question.GetAnswer(_gamemanager.GetQuestion().answer);
		}

		if(ph == phase.END) {
			_end.Q = "FINAL SCORE";
			_end.ansA = "Player 1\n" + players[0].points + " pts";

			if(_gamemanager.GetPlayerAmount() >= 2)
				_end.ansB = "Player 2\n" + players[1].points + " pts";
			else
				_end.ansB = "Player 2\n" + "NO PARTICIPANT";
			
			if(_gamemanager.GetPlayerAmount() >= 3)
				_end.ansX = "Player 3\n" + players[2].points + " pts";
			else
				_end.ansX = "Player 3\n" + "NO PARTICIPANT";
			
			if(_gamemanager.GetPlayerAmount() == 4)
				_end.ansY = "Player 4\n" + players[3].points + " pts";
			else
				_end.ansY = "Player 4\n" + "NO PARTICIPANT";

			question.Display(_end);
		}
	}

	bool AllPlayersReady()
	{
		int p = 0;
		for(int i=0; i < _gamemanager.GetPlayerAmount(); i++) {
			if(players[i].answer.text != " ")
				p ++;
		}

		return p == _gamemanager.GetPlayerAmount();
	}

	string GetWinner()
	{
		int w = 0;
		
		if(_gamemanager.GetPlayerAmount() > 1) {
			for(int i=1; i < _gamemanager.GetPlayerAmount(); i++) {
				if(players[w].points <= players[i].points)
					w = i;
			}
		}

		return "Player " + (w+1);
	}
}
