﻿using UnityEngine;
using System.IO;
using System.Text;
using System.Collections.Generic;

public enum ans { NONE, A, B, X, Y, };

[System.Serializable]
public struct Question
{
	public string name;
	public string Q;
	public string ansA;
	public string ansB;
	public string ansX;
	public string ansY;
	public ans answer;

	public void ConvertAnswer(string a) {
        answer = (ans)System.Enum.Parse(typeof(ans), a);
    }

	public bool IsCorrect(ans a) {
        return a == answer;
    }
}

[System.Serializable]
public struct Questionaire
{
    public List<Question> list;
    private Question _reader;
    private int _index;

    public void Init()
    {
        _index = 0;
        list = new List<Question>();
    }

    public void Init(string filename)
    {
        _index = 0;
        list = new List<Question>();
        GetQuestions(filename);
    }

    public void GetQuestions(string filename)
    {
        StreamReader _file = new StreamReader(filename, Encoding.Default);

        using (_file)
        {
            _reader.Q = _file.ReadLine();
            while(_reader.Q != null)
            {
                _reader.name = "Q" + _index;
                _reader.ansA = _file.ReadLine();
                _reader.ansB = _file.ReadLine();
                _reader.ansX= _file.ReadLine();
                _reader.ansY = _file.ReadLine();
                _reader.ConvertAnswer(_file.ReadLine());
                list.Add(_reader);

                _index ++;
                _reader.Q = _file.ReadLine();
            }
        }

        _file.Close();
    }

    public void GetQuestions(List<Question> getlist)
    {
		_index = getlist.Count;
		for(int i=0; i < getlist.Count; i++)
			list.Add(getlist[i]);
    }

    public void Randomize()
    {
        List<Question> newlist = new List<Question>();
        
        while(list.Count != 0)
        {
            int rand = Random.Range(0, list.Count);
            newlist.Add(list[rand]);
            list.Remove(list[rand]);
        }

        list = newlist;
    }

	public void LengthQuestionaire(int len)
	{
		Randomize();
		while(_index > len) {
			list.Remove(list[list.Count-1]);
			_index --;
		}
	}

	public void ClearList() {
		while(list.Count != 0)
			list.Remove(list [0]);
	}

	public void Next() { list.Remove(list[0]); }
	public Question GetCurrent() { return list[0]; }
}

public class QuestionManager : MonoBehaviour
{
	public Questionaire currentquestions;
	private Questionaire _normal;
	private Questionaire _mathematic;
	private Questionaire _holiday;
	private Questionaire _sports;
	private Questionaire _everything;
	private Questionaire _studymode;
    private int _length;

	public void Initialize()
	{
        /* Sort Questions */
        currentquestions.Init();
		_normal.Init("Questionaire/Normal.txt");
		_mathematic.Init("Questionaire/Mathematic.txt");
		_holiday.Init("Questionaire/Holiday.txt");
		_sports.Init("Questionaire/Sports.txt");
		_studymode.Init("Questionaire/StudyGuide.txt");

        /* Everything and Anything */
        _everything.Init();
		_everything.GetQuestions("Questionaire/Normal.txt");
		_everything.GetQuestions("Questionaire/Mathematic.txt");
		_everything.GetQuestions("Questionaire/Holiday.txt");
		_everything.GetQuestions("Questionaire/Sports.txt");
        _everything.GetQuestions("Questionaire/Additional.txt");
	}

	public void NextQuestion() { currentquestions.Next(); }

    public void SetLength(int l) {
		if(l != -1)
			_length = l;
		else if(l == -1)
			_length = _studymode.list.Count;
	}

    public void SetQuestionaire(int set)
    {
        if(set == 0)
            currentquestions.GetQuestions(_normal.list);

        if(set == 1)
            currentquestions.GetQuestions(_mathematic.list);

        if(set == 2)
            currentquestions.GetQuestions(_holiday.list);

        if(set == 3)
            currentquestions.GetQuestions(_sports.list);

        if(set == 4)
            currentquestions.GetQuestions(_everything.list);

		if(set == 5)
			currentquestions.GetQuestions(_studymode.list);

		currentquestions.LengthQuestionaire(_length);
    }

	public int GetLength() { return _length; }
	public Question GetCurrentQuestion() { return currentquestions.GetCurrent(); }
	public void ClearCurrentQuestionaire() { currentquestions.ClearList(); }
}
