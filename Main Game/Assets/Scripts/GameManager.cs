﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
	void Awake() {
		DontDestroyOnLoad(gameObject);
		Cursor.visible = false;
	}

	/* Game Mode */
	public enum type { NONE, NORMAL, CHAOTIC, STUDY, }
	public type gametype = type.NONE;

	/* Game State */
	public enum state { NONE, MENU, PLAY, PAUSE, END, }
	public state gamestate = state.NONE;

	/* Managers */
	private QuestionManager _qm;
	private int _playeramount;

	/* Variables */
	private int _timer;

	/* Initialization */
	public void Initialize()
	{
		_qm = gameObject.GetComponent<QuestionManager>();
		if(!_qm) {
			print("[Initialize]ERROR: cannot find Question Manager");
			return;
		}

		_qm.Initialize();
		_timer = 0;
		_playeramount = 0;
		gametype = type.NONE;
		gamestate = state.MENU;
	}

	/* ADDITIONAL FUNCTIONS */
	public bool Delay(int d)
	{
		_timer ++;
		if(_timer >= (d*60)) {
			_timer = 0;
			return true;
		}
		
		return false;
	}

	public void NextQuestion() { _qm.NextQuestion(); }

	/* SET FUNCTIONS */
	public void SetMode(type m) { gametype = m; }
	public void ChangeGameState(state s) { gamestate = s; }
	public void SetQuestionLength(int l) { _qm.SetLength(l); }
	public void SetPlayerAmount(int num) { _playeramount = num; }
    public void SetQuestionaire(int set) { _qm.SetQuestionaire(set); }
	public void PauseGame() {
		if(gamestate != state.END)
		{
			if(isPaused())
				gamestate = state.PLAY;
			else
				gamestate = state.PAUSE;
		}
	}

	/* GET FUNCTIONS */
	public int GetQuestionLength() { return _qm.GetLength(); }
	public int GetPlayerAmount() { return _playeramount; }
	public bool isPaused() { return gamestate == state.PAUSE; }
    public Question GetQuestion() { return _qm.GetCurrentQuestion(); }
	public void ClearQuestionaire() { _qm.ClearCurrentQuestionaire(); }
}