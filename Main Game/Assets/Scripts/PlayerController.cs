﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	public int playernum = 0;
	public ans playeranswer = ans.NONE;
	public GameManager _gamemanager;
	public AudioSource audio;

	void Start() { _gamemanager = GameObject.Find("GameManager").GetComponent<GameManager>(); }
	void Update()
	{
		if(Input.GetButtonDown ("p" + playernum + "Start"))
			_gamemanager.PauseGame();

        if (!_gamemanager.isPaused() && playeranswer == ans.NONE
		    && _gamemanager.gamestate == GameManager.state.PLAY)
        {
            if (Input.GetButtonDown("p" + playernum + "A")) {
                playeranswer = ans.A;
				audio.Play();
			}

            if (Input.GetButtonDown("p" + playernum + "B")) {
				playeranswer = ans.B;
				audio.Play();
			}

            if (Input.GetButtonDown("p" + playernum + "X")) {
				playeranswer = ans.X;
				audio.Play();
			}

            if (Input.GetButtonDown("p" + playernum + "Y")) {
				playeranswer = ans.Y;
				audio.Play();
			}
        }
	}
}