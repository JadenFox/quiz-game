﻿using UnityEngine;
using System.Collections;

public class Initialization : MonoBehaviour
{
	public int timer = 1;
	private GameManager _gm;

	void Start()
	{
		_gm = GameObject.Find("GameManager").GetComponent<GameManager>();
		_gm.Initialize();
	}

	void Update()
	{
		if(_gm.Delay(timer))
			Application.LoadLevel(1);
	}
}
