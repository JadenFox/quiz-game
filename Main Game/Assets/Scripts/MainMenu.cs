﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public struct Start
{
	public AudioSource audio;
    public Canvas can;
    public Image[] menu;
    public Sprite normal;
	public Sprite selected;
	public Sprite chosen;
	private bool _pressed;
	private int _index;

    public void Init(bool active)
    {
		can.gameObject.SetActive(active);
		for(int i = 0; i < menu.Length; i++)
			menu[i].sprite = normal;
		menu[menu.Length-1].sprite = normal;
		_pressed = false;
		_index = menu.Length-1;
    }

    public void Run()
    {
		if(!_pressed)
		{
			for (int i = 0; i < menu.Length; i++)
				menu[i].sprite = normal;
			menu[_index].sprite = selected;
		}
		
		if(Input.GetButtonDown("p1Y"))
		{
			_index --;
			if (_index < 0)
				_index = menu.Length-1;
		}
		
		else if(Input.GetButtonDown("p1A"))
		{
			_index ++;
			if(_index >= menu.Length)
				_index = 0;
		}
		
		else if (Input.GetButtonDown("p1Start"))
		{
			_pressed = true;
			audio.Play();
			menu[_index].sprite = chosen;
		}
    }

	public bool isPressed() { return _pressed; }
	public int getIndex() { return _index; }
};

[System.Serializable]
public struct Main
{
	public AudioSource audio;
    public Canvas can;
    public Image[] menu;
    public Sprite normal;
    public Sprite selected;
    public Sprite chosen;
    private bool _pressed;
    private int _index;

    public void Init(bool active)
    {
        can.gameObject.SetActive(active);
        for(int i = 0; i < menu.Length; i++)
            menu[i].sprite = normal;
        menu[0].sprite = selected;

        _pressed = false;
        _index = 0;
    }

    public void Run()
    {
        if(!_pressed)
        {
            for (int i = 0; i < menu.Length; i++)
                menu[i].sprite = normal;
            menu[_index].sprite = selected;
        }

		if(Input.GetButtonDown("p1Y"))
        {
            _index --;
            if (_index < 0)
                _index = menu.Length-1;
        }

		else if(Input.GetButtonDown("p1A"))
        {
            _index ++;
            if(_index >= menu.Length)
                _index = 0;
        }

		else if (Input.GetButtonDown("p1Start"))
        {
			_pressed = true;
			audio.Play();
            menu[_index].sprite = chosen;
        }
    }

    public bool isPressed() { return _pressed; }
    public int getIndex() { return _index; }
};

[System.Serializable]
public struct Display
{
	public Canvas can;
	public Text load;
	private int _timer;
	
	public void Init(bool active)
	{
		can.gameObject.SetActive(active);
		load.text = "LOADING";
		_timer = 0;
	}
	
	public void Run()
	{
		_timer ++;
		if(_timer == 10 || _timer == 20 || _timer == 30)
			load.text += ".";
		else if(_timer == 40) {
			load.text = "LOADING";
			_timer = 0;
		}
	}
};

[System.Serializable]
public struct HowTo
{
	public Text txt;
	public Canvas can;
	public GameObject L;
	public GameObject R;
	public GameObject[] menu;
    private int _index;
	
	public void Init(bool active)
	{
		txt.text = "Quizcall Unlimited\nHow to Play";
		can.gameObject.SetActive(active);
		L.SetActive(false);
		R.SetActive(false);
		_index = 0;
	}
	
	public void Run()
	{
		txt.text = "Quizcall Unlimited\nHow to Play";
		foreach(GameObject obj in menu)
			obj.SetActive(false);
		menu[_index].SetActive(true);

		if(_index == (menu.Length-1))
			txt.text = "Quizcall Unlimited\nCredit";

		L.SetActive(true);
		if(_index == 0)
			L.SetActive(false);

		R.SetActive(true);
		if(_index == (menu.Length-1))
			R.SetActive(false);

		if(Input.GetButtonDown("p1X"))
		{
			if (_index > 0)
				_index --;
		}
		
		else if(Input.GetButtonDown("p1B"))
		{
			if(_index < (menu.Length-1))
				_index ++;
		}
	}
};

public class MainMenu : MonoBehaviour
{
    private enum state { START, MODE, LENGTH, TYPE, PLAYER, GAME, HOWTOPLAY, }
    private GameManager _gamemanager;
    private state _menustate;

    public Start start;
    public Main mode;
    public Main length;
    public Main type;
	public Main player;
	public Display game;
	public HowTo howtoplay;

    void Awake()
    {
        _menustate = state.START;
        _gamemanager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    void Start()
    {
		_gamemanager.ChangeGameState(GameManager.state.MENU);
        start.Init(true);
        mode.Init(false);
        length.Init(false);
        type.Init(false);
		player.Init(false);
		game.Init(false);
		howtoplay.Init(false);
    }

    void Update()
    {
		if(Input.GetButtonDown ("Back")) {
			Back();
		}

        switch (_menustate)
        {
            case state.START:
                start.Run();
                if (start.isPressed()) {
					if(_gamemanager.Delay(1))
					{
						if(start.getIndex() == 0)
							ChangeState(state.MODE);
						else
							ChangeState(state.HOWTOPLAY);
					}
                }

                break;

            case state.MODE:
                mode.Run();
                if(mode.isPressed())
                {
					if(_gamemanager.Delay(1))
                    {
                        if(mode.getIndex() == 0)
                            _gamemanager.SetMode(GameManager.type.NORMAL);
                        else
                            _gamemanager.SetMode(GameManager.type.CHAOTIC);
                        ChangeState(state.LENGTH);
                    }
                }
                break;

            case state.LENGTH:
                length.Run();
                if (length.isPressed())
                {
					if(_gamemanager.Delay(1))
                    {
                        if(length.getIndex() == 0)
                            _gamemanager.SetQuestionLength(5);
                        else if (length.getIndex() == 1)
                            _gamemanager.SetQuestionLength(20);
                        else if (length.getIndex() == 2)
                            _gamemanager.SetQuestionLength(40);

                        ChangeState(state.TYPE);
                    }
                }
                break;

            case state.TYPE:
                type.Run();
                if (type.isPressed())
                {
					if(_gamemanager.Delay(1))
                    {
						if(type.getIndex() != 5) {
	                        _gamemanager.SetQuestionaire(type.getIndex());
							ChangeState(state.PLAYER);
						}
						else if(type.getIndex() == 5) {
							_gamemanager.SetMode(GameManager.type.STUDY);
							_gamemanager.SetQuestionaire(type.getIndex());
							_gamemanager.SetQuestionLength(-1);
							_gamemanager.SetPlayerAmount(1);
							ChangeState(state.GAME);
						}
                    }
	            }
				break;
				
			case state.PLAYER:
				player.Run();
				if(player.isPressed())
				{
					if(_gamemanager.Delay(1))
					{
						_gamemanager.SetPlayerAmount(player.getIndex()+1);
						ChangeState(state.GAME);
					}
				}
				break;
				
			case state.GAME:
				game.Run();
				if(_gamemanager.Delay(4))
				{
					_gamemanager.gamestate = GameManager.state.PLAY;
					Application.LoadLevel(2);
				}
				break;
				
			case state.HOWTOPLAY:
				howtoplay.Run();
				break;
        }
    }

	void Back()
	{
		if(_menustate == state.START)
			Application.Quit();

		else if(_menustate == state.MODE)
			ChangeState (state.START);
		
		else if (_menustate == state.LENGTH)
			ChangeState (state.MODE);
		
		else if (_menustate == state.TYPE)
			ChangeState (state.LENGTH);
		
		else if (_menustate == state.PLAYER)
			ChangeState (state.TYPE);
		
		else if(_menustate == state.HOWTOPLAY)
			ChangeState (state.START);
	}

    void ChangeState(state s)
	{
		_menustate = s;
        if(s == state.START)
        {
            start.Init(true);
            mode.Init(false);
            length.Init(false);
			type.Init(false);
			player.Init(false);
			game.Init(false);
			howtoplay.Init(false);
        }

        if (s == state.MODE)
        {
            start.Init(false);
            mode.Init(true);
            length.Init(false);
			type.Init(false);
			player.Init(false);
			game.Init(false);
			howtoplay.Init(false);
        }

        if (s == state.LENGTH)
        {
            start.Init(false);
            mode.Init(false);
            length.Init(true);
			type.Init(false);
			player.Init(false);
			game.Init(false);
			howtoplay.Init(false);
        }

        if (s == state.TYPE)
        {
            start.Init(false);
            mode.Init(false);
            length.Init(false);
			type.Init(true);
			player.Init(false);
			game.Init(false);
			howtoplay.Init(false);
		}
		
		if (s == state.PLAYER)
		{
			start.Init(false);
			mode.Init(false);
			length.Init(false);
			type.Init(false);
			player.Init(true);
			game.Init(false);
			howtoplay.Init(false);
		}

        if (s == state.GAME)
        {
            start.Init(false);
            mode.Init(false);
            length.Init(false);
			type.Init(false);
			player.Init(false);
			game.Init(true);
			howtoplay.Init(false);
		}

		if (s == state.HOWTOPLAY)
		{
			start.Init(false);
			mode.Init(false);
			length.Init(false);
			type.Init(false);
			player.Init(false);
			game.Init(false);
			howtoplay.Init(true);
		}
    }
}
